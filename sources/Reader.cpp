/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Reader.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/28 13:14:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/06/03 21:48:12 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>
#include <iostream>

#include "Reader.hpp"

/*!
 * Trims the string from both ends in place.
 * @param s string to be trimmed.
 */
static inline void trim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch)
                                    { return !std::isspace(ch); }));
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch)
                         { return !std::isspace(ch); }).base(), s.end());
}

static std::string GetStreamContent(std::istream& is, const bool checkEnd = false)
{
    std::string result;

    for (bool finish = false; !is.eof() && !finish;)
    {
        std::string line;
        std::getline(is, line);

        if (checkEnd && line.find(";;") != std::string::npos)
        {
            finish = true;
            line.erase(line.find(";;"));
        }

        result += line + '\n';
    }
    trim(result);
    return result;
}

std::string Reader::GetFileContent(const std::string& filename)
{
    if (filename.empty())
        return GetStreamContent(std::cin, true);
    else
    {
        std::ifstream file(filename);
        if (!file.is_open() || file.fail())
            throw Exception(file.is_open() ? "Bad file." : "Cannot open the file.");
        return GetStreamContent(file);
    }
}

Reader::Exception::Exception(const char* what_arg)
    : std::runtime_error(what_arg)
{}

Reader::Exception::Exception(const std::string& what_arg)
    : std::runtime_error(what_arg)
{}
