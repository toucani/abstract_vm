/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ArgumentsTest.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/26 12:31:37 by dkovalch          #+#    #+#             */
/*   Updated: 2018/05/28 13:38:09 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>

#include "Arguments.hpp"

TEST(Arguments, BasicParse)
{
    const char *argv[2] =
    {
        "a.out", nullptr
    };
    //It must not fail despite wrong argc
    Arguments args(5, argv);
    EXPECT_FALSE(args.Help());
    EXPECT_EQ(args.Filename(), "");
}

TEST(Arguments, HelpParse)
{
    const char *Vav[2][3] =
    {
        { "a.out", "-h", nullptr },
        { "a.out", "--help", nullptr },
    };

    for(auto i = 0; i < 1 ; i++)
    {
        Arguments args(-1, Vav[i]);
        EXPECT_TRUE(args.Help());
        EXPECT_EQ(args.Filename(), "");
    }
}

TEST(Arguments, FilenameParse)
{
    const char *Vav[2][4] =
    {
        { "a.out", "-f", "ffile", nullptr },
        { "a.out", "--file=ffile", nullptr }
    };

    for(auto i = 0; i < 2 ; i++)
    {
        Arguments args(-1, Vav[i]);
        EXPECT_FALSE(args.Help());
        EXPECT_EQ(args.Filename(), "ffile");
    }
}

TEST(Arguments, Invalid)
{
    const char *Iav[8][5] =
    {
        { nullptr },
        { "a.out", "-f", nullptr },
        { "a.out", "--file=", nullptr },
        { "a.out", "-e", "-q", "-p", nullptr },
        { "a.out", "--heuristic=t", "--size=t", "--shuffle=o", nullptr },
        { "a.out", "--solvable=", "-file=ffile", "--s ", nullptr},
        { "a.out", "wrwe", "wfe321", "54632", nullptr },
        { "a.out", "--algq=trtr", "-a", "eqwe", nullptr }
    };

    for(auto i = 0; i < 8 ; i++)
    {
        try
        {
            Arguments args(-1, Iav[i]);
            EXPECT_FALSE(args.Help());
            EXPECT_EQ(args.Filename(), "");
        }
        catch(Arguments::Exception& ex)
        {
        }
    }
}
