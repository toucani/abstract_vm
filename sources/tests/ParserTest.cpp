/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ParserTest.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/01 21:07:29 by dkovalch          #+#    #+#             */
/*   Updated: 2018/06/25 21:16:14 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <array>
#include <gtest/gtest.h>

#include "Parser.hpp"

const std::string postCode = "\nexit";
const std::string dummyCode = "push int8(8)\npush int8(2)\n";
const std::array<std::string, 4> Blanks {{" ", "\t", " \t ", "\t \t"}};

const std::array<std::string, 3> IntTypes {{"int8", "int16", "int32"}};
const std::array<std::string, 6> IntValues {{"-1", "-0", "0", "+0", "+1", "1"}};

const std::array<std::string, 2> FloatTypes {{"float", "double"}};
const std::array<std::string, 18> FloatValues
{{
    "-1.7", "-1.0", "-1", "-0.7",
    "-0.0", "-0", "0", "0.0", "+0", "+0.0",
    "+0.7", "0.7", "+1", "1", "+1.0", "1.0", "+1.7", "1.7"
}};

const std::array<std::string, 8> Commands
{{
    "pop", "add", "sub", "mul", "mod", "div", "dump", "print"
}};

#define EXPECT_OK(code) EXPECT_EXIT(Parser::Parse(code), testing::ExitedWithCode(EXIT_SUCCESS), "")

TEST(Parser, Commands)
{
    for (const auto& command : Commands)
        for (const auto& blank : Blanks)
        {
            EXPECT_OK(dummyCode + blank + command + blank + postCode);
            EXPECT_OK(dummyCode + blank + command + blank + "; poney" + postCode);
            EXPECT_OK(dummyCode + blank + command + " ; poney" + blank + postCode);
            EXPECT_OK(dummyCode + blank + command + " ; poney" + blank + "; poney" + postCode);
        }
    EXPECT_OK("exit;poney");
}

TEST(Parser, PushInt)
{
    for (const auto& type : IntTypes)
        for (const auto& value : IntValues)
            for (const auto& l_blank : Blanks)
            for (const auto& r_blank : Blanks)
            {
                EXPECT_OK("push" + l_blank + type + "(" + r_blank + value + r_blank + ")" + postCode);
            }
}

TEST(Parser, PushFloat)
{
    for (const auto& type : FloatTypes)
        for (const auto& value : FloatValues)
            for (const auto& l_blank : Blanks)
            for (const auto& r_blank : Blanks)
            {
                EXPECT_OK("push" + l_blank + type + "(" + r_blank + value + r_blank + ")" + postCode);
            }
}

TEST(Parser, AssertInt)
{
    for (const auto& type : IntTypes)
        for (const auto& value : IntValues)
            for (const auto& l_blank : Blanks)
            for (const auto& r_blank : Blanks)
            {
                const auto str = l_blank + type + "(" + r_blank + value + r_blank + ")\n";
                EXPECT_OK("push" + str + "assert" + str + postCode);
            }
}

TEST(Parser, AssertFloat)
{
    for (const auto& type : FloatTypes)
        for (const auto& value : FloatValues)
            for (const auto& l_blank : Blanks)
            for (const auto& r_blank : Blanks)
            {
                const auto str = l_blank + type + "(" + r_blank + value + r_blank + ")\n";
                EXPECT_OK("push" + str + "assert" + str + postCode);
            }
}
