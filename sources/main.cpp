/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/26 09:03:20 by dkovalch          #+#    #+#             */
/*   Updated: 2018/06/25 20:30:30 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "Stack.hpp"
#include "Parser.hpp"
#include "Reader.hpp"
#include "Arguments.hpp"

int main(int argc, const char **argv)
{
    try
    {
        Arguments args(argc, argv);
        if (args.Help())
            Arguments::PrintHelp();
        else
            Parser::Parse(Reader::GetFileContent(args.Filename()));
        exit(EXIT_SUCCESS);
    }
    catch (Arguments::Exception& ex)
    {
        std::cerr << "Argument error: " << ex.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    catch (Reader::Exception& ex)
    {
        std::cerr << "File error: " << ex.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    catch (Parser::Exception& ex)
    {
        std::cerr << "Parse error: " << ex.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    catch (Stack::Exception& ex)
    {
        std::cerr << "Runtime error: " << ex.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    catch (std::exception& ex)
    {
        std::cerr << "Fatal error: " << ex.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    exit(EXIT_SUCCESS);
}
