/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Parser.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/28 18:05:39 by dkovalch          #+#    #+#             */
/*   Updated: 2018/06/25 20:31:11 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <tao/pegtl.hpp>
#include <tao/pegtl/analyze.hpp>

#include "Parser.hpp"
#include "Grammar.hpp"
#include "Actions.hpp"

namespace pegtl = tao::pegtl;

void Parser::Parse(const std::string& code)
{
    assert(tao::pegtl::analyze<Grammar::Code>() == 0);

    std::string source;
    tao::pegtl::string_input<> in(code, source);
    try
    {
        tao::pegtl::parse<Grammar::Code, Grammar::Action>(in);
    }
    catch(tao::pegtl::parse_error& err)
    {
        std::stringstream ss;
        const auto position = err.positions.front();
        ss
           << "line " << position.line << ", position "
           << position.byte_in_line << "\n\t>"
           << in.line_at(position) << "<\n\t"
           << std::string(position.byte_in_line, ' ') << "^";
        throw Exception(ss.str());
    }
}

Parser::Exception::Exception(const std::string &what_arg)
    : runtime_error(what_arg)
{}

Parser::Exception::Exception(const char *what_arg)
    : runtime_error(what_arg)
{}
