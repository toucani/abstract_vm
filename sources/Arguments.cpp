/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Arguments.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/26 09:16:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/05/28 17:55:12 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <sstream>
#include <iostream>
#include <stdexcept>

#include "Arguments.hpp"

/*!
 * Trims the string from both ends in place.
 * @param s string to be trimmed.
 */
static inline void trim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch)
                                    { return !std::isspace(ch); }));
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch)
                         { return !std::isspace(ch); }).base(), s.end());
}

/*!
 * Parses the arguments and fills the structure.
 * Starts from 1, skipping the program name.
 * @param argc number of arguents from main(argc, argv)
 * @param argv null-terminated arguments from main(argc, argv)
 */
Arguments::Arguments(int argc, const char **argv)
{
    std::vector<std::string> args;
    for (int ct = 1; (argc < 0 || ct < argc) && argv[ct]; ct++)
    {
        args.emplace_back(argv[ct]);
        trim(args.back());
    }

    _help = GetFlagValue(args, "-h", "--help", false);
    _filename = GetArgValue(args, "-f", "--file");
}

bool Arguments::Help() const
{
    return _help;
}

const std::string& Arguments::Filename() const
{
    return _filename;
}

void Arguments::PrintHelp()
{
    std::cout
       << "NAME\n\t"
       << "abstract_vm is a simple asm-like language parser and executioner.\n\n"
       << "SYNOPSIS\n\t"
       << "abstract_vm [-h] [--help] [-f file] [--file=file]\n\n"
       << "DESCRIPTION\n\t"
       << "abstract_vm reads and executes your program line-by-line,\n\t"
       << "holding all values in a stack, so adding a value will put it\n\t"
       << "on top of the stack, while removing a value, will pop the top value.\n\n\t"
       << "Long options have priority over short ones, last options have priority over first ones.\n\n\t"
       << "-h, --help\tprints this message.\n\t"
       << "-f, --file\tloads a program from the file.\n\n"
       << "FILE\n\t"
       << "You can specify a file to read instrutions from, here are all possible instructions:\n\n\t"
       << "push VALUE\tAdd specified value on top of the stack\n\t"
       << "pop\t\tRemove top value from the stack\n\t"
       << "dump\t\tPrint all the values in the stack\n\t"
       << "assert VALUE\tCheck that top value is VALUE\n\t"
       << "add\t\tPop 2 top values, add them, and push the result\n\t"
       << "sub\t\tPop 2 top values, substract them, and push the result\n\t"
       << "mul\t\tPop 2 top values, multiply them, and push the result\n\t"
       << "div\t\tPop 2 top values, divide them, and push the result\n\t"
       << "mod\t\tPop 2 top values, modulo them, and push the result\n\t"
       << "print\t\tPrint top value as if it was an ASCII character\n\t"
       << "exit\t\tStop execution and exit\n";
}

/*!
 * Looks for the flag in command args. Returns true if present, defaultValue otherwise.
 * @param args arguments from command line
 * @param shortOpt option to look for in "-x" format
 * @param longOpt option to look for in "--xxxx" format
 * @param defaultValue returned value if option is not found.
 * @return true if found, default value otherwise.
 */
bool Arguments::GetFlagValue(const std::vector<std::string>& args,
                             const std::string& shortOpt,
                             const std::string& longOpt,
                             bool defaultValue)
{
    return std::find_if(args.begin(), args.end(),
                        [shortOpt, longOpt](const auto& item)
                        { return item == shortOpt || item == longOpt; }
    ) != args.end() ? true : defaultValue;
}

/*!
 * Looks for the parameter in command args. Returns string after = if present, defaultValue otherwise.
 * @param args arguments from command line
 * @param shortOpt option to look for in "-x" format
 * @param longOpt option to look for in "--xxxx=" format
 * @param defaultValue returned value if option is not found.
 * @return returns string after = in "--xxxx=qqqqq" format or next argument in "-x" format.
 * If there is no such argument, defaultValue is returned.
 */
std::string Arguments::GetArgValue(const std::vector<std::string>& args,
                                   const std::string& shortOpt,
                                   const std::string& longOpt,
                                   const std::string& defaultValue)
{
    const auto it = std::find_if(args.begin(), args.end(),
        [shortOpt, longOpt](const auto& item)
        {
            return item == shortOpt || item.find(longOpt) != std::string::npos;
        });

    if (it == args.end())
        return defaultValue;

    if ((*it == shortOpt || it->find_last_of('=') == std::string::npos)
        && it + 1 != args.end() && !(it + 1)->empty() && (*(it + 1))[0] != '-')
        return *(it + 1);

    const auto pos = it->find_last_of('=');

    //Since the short option has already been checked,
    //no sense to check for the long one here.
    if (pos < it->length() - 1)
        return it->substr(pos + 1);

    throw Exception("Missing argument for " + *it);
}

Arguments::Exception::Exception(const char* what_arg)
    : std::runtime_error(what_arg)
{}

Arguments::Exception::Exception(const std::string& what_arg)
    : std::runtime_error(what_arg)
{}
