/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Factory.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/03 13:46:18 by dkovalch          #+#    #+#             */
/*   Updated: 2020/05/10 16:21:34 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Operand.hpp"
#include "Factory.hpp"

const std::array<IOperand*(*)(), 5> Factory::_builders =
{{
    &Factory::BuildInt8,
    &Factory::BuildInt16,
    &Factory::BuildInt32,
    &Factory::BuildFloat,
    &Factory::BuildDouble
}};

Factory& Factory::GetInstance()
{
    static Factory f;
    return f;
}

IOperand* Factory::CreateOperand(IOperand::Type type, std::string value)
{
    IOperand* op = _builders[enum_cast(type)]();
    op->FromString(value);
    return op;
}
IOperand* Factory::CreateOperand(const IOperand* lhs, const IOperand* rhs)
{
    IOperand::Type type = IOperand::Type(std::max(enum_cast(lhs->GetType()), enum_cast(rhs->GetType())));
    return _builders[enum_cast(type)]();
}

IOperand *Factory::BuildInt8()
{
    return new Int8Operand();
}

IOperand *Factory::BuildInt16()
{
    return new Int16Operand();
}

IOperand *Factory::BuildInt32()
{
    return new Int32Operand();
}

IOperand *Factory::BuildFloat()
{
    return new FloatOperand();
}

IOperand *Factory::BuildDouble()
{
    return new DoubleOperand();
}
