/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Stack.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/02 11:51:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/06/03 21:27:47 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Stack.hpp"

Stack& Stack::GetInstance()
{
    static Stack s;
    return s;
}

void Stack::push(IOperand* op)
{
    _data.emplace_back(op);
}

std::unique_ptr<IOperand> Stack::pop()
{
    if (_data.empty())
        throw Exception("empty stack!");
    auto res = std::move(_data.back());
    _data.pop_back();
    return res;
}

IOperand* Stack::top()
{
    if (_data.empty())
        throw Exception("empty stack!");
    return _data.back().get();
}

const IOperand* Stack::top() const
{
    if (_data.empty())
        throw Exception("empty stack!");
    return _data.back().get();
}

bool Stack::empty() const
{
    return _data.empty();
}

Stack::container::size_type Stack::size() const
{
    return _data.size();
}

std::ostream& operator<<(std::ostream& os, const Stack& s)
{
    for (auto element = s._data.rbegin(); element != s._data.rend(); element++)
        os << **element << "\n";
    return os;
}

Stack::Exception::Exception(const std::string &what_arg)
    : runtime_error(what_arg)
{}

Stack::Exception::Exception(const char *what_arg)
    : runtime_error(what_arg)
{}
