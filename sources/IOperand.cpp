/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IOperand.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/03 13:18:22 by dkovalch          #+#    #+#             */
/*   Updated: 2018/06/03 21:45:14 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "IOperand.hpp"

bool IOperand::operator!=(const IOperand& other) const
{
    return !(*this == other);
}

bool IOperand::operator==(const IOperand& other) const
{
    return GetType() == other.GetType() && ToString() == other.ToString();
}

std::ostream& operator<<(std::ostream& os, const IOperand& op)
{
    os << op.ToString();
    return os;
}

std::ostream& operator<<(std::ostream& os, const IOperand* op)
{
    os << *op;
    return os;
}
