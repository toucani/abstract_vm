/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Actions.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/03 21:30:14 by dkovalch          #+#    #+#             */
/*   Updated: 2018/06/04 21:19:37 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Grammar.hpp"
#include "Operand.hpp"
#include "Stack.hpp"

namespace Grammar
{
    template<typename Rule>
    struct Action : tao::pegtl::nothing<Rule> {};

    template<>
    struct Action<Commands::Pop>
    {
        template<typename Input>
        static void apply(const Input&)
        {
            STACK.pop();
        }
    };

    template<>
    struct Action<Commands::Dump>
    {
        template<typename Input>
        static void apply(const Input&)
        {
            std::cout << STACK << std::endl;
        }
    };

    template<>
    struct Action<Commands::Print>
    {
        template<typename Input>
        static void apply(const Input&)
        {
            if (STACK.top()->GetType() != IOperand::Type::Int8)
                throw Stack::Exception("only int8 values are printable.");
            std::cout << static_cast<char>(std::stoi(STACK.top()->ToString()));
        }
    };

    template<>
    struct Action<Commands::Exit>
    {
        template<typename Input>
        static void apply(const Input&)
        {
            exit(EXIT_SUCCESS);
        }
    };

    template<>
    struct Action<Commands::Add>
    {
        template<typename Input>
        static void apply(const Input&)
        {
            if (STACK.size() < 2)
                throw Stack::Exception("not enough operands.");
            STACK.push(*STACK.pop() + *STACK.pop());
        }
    };

    template<>
    struct Action<Commands::Sub>
    {
        template<typename Input>
        static void apply(const Input&)
        {
            if (STACK.size() < 2)
                throw Stack::Exception("not enough operands.");
            auto op1 = STACK.pop();
            STACK.push(*STACK.pop() - *op1);
        }
    };

    template<>
    struct Action<Commands::Mul>
    {
        template<typename Input>
        static void apply(const Input&)
        {
            if (STACK.size() < 2)
                throw Stack::Exception("not enough operands.");
            auto op1 = STACK.pop();
            STACK.push(*STACK.pop() * *op1);
        }
    };

    template<>
    struct Action<Commands::Div>
    {
        template<typename Input>
        static void apply(const Input&)
        {
            if (STACK.size() < 2)
                throw Stack::Exception("not enough operands.");
            auto op1 = STACK.pop();
            if (op1->ToString() == "0" || op1->ToString() == "0.000000")
                throw Stack::Exception("division by zero.");
            STACK.push(*STACK.pop() / *op1);
        }
    };

    template<>
    struct Action<Commands::Mod>
    {
        template<typename Input>
        static void apply(const Input&)
        {
            if (STACK.size() < 2)
                throw Stack::Exception("not enough operands.");
            auto op1 = STACK.pop();
            if (op1->ToString() == "0" || op1->ToString() == "0.000000")
                throw Stack::Exception("modulo by zero.");
            STACK.push(*STACK.pop() % *op1);
        }
    };

    template<>
    struct Action<Commands::Push>
    {
        template<typename Input>
        static void apply(const Input& in)
        {
            const auto& line = in.string();
            if (line.find("int8") != std::string::npos)
            {
                STACK.push(new Int8Operand());
            }
            else if (line.find("int16") != std::string::npos)
            {
                STACK.push(new Int16Operand());
            }
            else if (line.find("int32") != std::string::npos)
            {
                STACK.push(new Int32Operand());
            }
            else if (line.find("float") != std::string::npos)
            {
                STACK.push(new FloatOperand());
            }
            else if (line.find("double") != std::string::npos)
            {
                STACK.push(new DoubleOperand());
            }
            STACK.top()->FromString(line.substr(line.find("(") + 1, line.find(")") - line.find("(") - 1));
        }
    };

    template<>
    struct Action<Commands::Assert>
    {
        template<typename Input>
        static void apply(const Input& in)
        {
            const auto& line = in.string();
            std::unique_ptr<IOperand> op;
            if (line.find("int8") != std::string::npos)
            {
                op.reset(new Int8Operand());
            }
            else if (line.find("int16") != std::string::npos)
            {
                op.reset(new Int16Operand());
            }
            else if (line.find("int32") != std::string::npos)
            {
                op.reset(new Int32Operand());
            }
            else if (line.find("float") != std::string::npos)
            {
                op.reset(new FloatOperand());
            }
            else if (line.find("double") != std::string::npos)
            {
                op.reset(new DoubleOperand());
            }
            op->FromString(line.substr(line.find("(") + 1, line.find(")") - line.find("(") - 1));
            if (op->GetType() != STACK.top()->GetType())
                throw Stack::Exception("assertion failed: type mismatch.");
            if (*op != *STACK.top())
                throw Stack::Exception("assertion failed: value mismatch: " + STACK.top()->ToString() + " is not " + op->ToString());
        }
    };
}
