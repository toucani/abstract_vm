/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Factory.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/03 13:40:28 by dkovalch          #+#    #+#             */
/*   Updated: 2020/05/10 16:21:33 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <array>
#include <string>
#include <functional>

#include "IOperand.hpp"

#define FACTORY Factory::GetInstance()

class Factory
{
    Factory() = default;
    Factory(Factory &&) = default;
    Factory(const Factory &) = default;

    Factory &operator=(Factory &&) = default;
    Factory &operator=(const Factory &) = default;

public:
    static Factory& GetInstance();

    IOperand* CreateOperand(IOperand::Type, std::string value = "");
    IOperand* CreateOperand(const IOperand*, const IOperand*);

private:
    static IOperand *BuildInt8();
    static IOperand *BuildInt16();
    static IOperand *BuildInt32();
    static IOperand *BuildFloat();
    static IOperand *BuildDouble();

    static const std::array<IOperand*(*)(), 5> _builders;
};
