/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Stack.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/02 11:51:00 by dkovalch          #+#    #+#             */
/*   Updated: 2020/05/10 16:22:07 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>
#include <vector>
#include <iostream>
#include <stdexcept>

#include "IOperand.hpp"

#define STACK Stack::GetInstance()

class Stack
{
    using container = std::vector<std::unique_ptr<IOperand>>;

    Stack() = default;
    Stack(Stack &&) = default;
    Stack(const Stack &) = default;

    Stack &operator=(Stack &&) = default;
    Stack &operator=(const Stack &) = default;

public:
    static Stack& GetInstance();

    void push(IOperand*);
    std::unique_ptr<IOperand> pop();

    IOperand* top();
    const IOperand* top() const;

    bool empty() const;
    container::size_type size() const;

    friend std::ostream& operator<<(std::ostream&, const Stack&);

    class Exception : public std::runtime_error
    {
    public:
        Exception(Exception&&) = default;
        Exception(const Exception&) = default;

        explicit Exception(const char* what_arg);
        explicit Exception(const std::string& what_arg);

        Exception& operator=(Exception&&) = default;
        Exception& operator=(const Exception&) = default;
    };

private:
    container _data;
};
