/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Operand.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/03 11:17:36 by dkovalch          #+#    #+#             */
/*   Updated: 2018/06/04 21:18:38 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <cstdint>
#include <sstream>
#include <iostream>

#include "Stack.hpp" //Exception
#include "Factory.hpp"
#include "IOperand.hpp"

template <typename T>
class Operand final : public IOperand
{
    static_assert(std::is_same<int8_t, T>::value
                || std::is_same<int16_t, T>::value
                || std::is_same<int32_t, T>::value
                || std::is_same<float, T>::value
                || std::is_same<double, T>::value,
        "T must be either int8_t, int16_t, int32_t, float or double!");

public:
    using type = T;

    Operand() = default;
    Operand(Operand&&) = default;
    Operand(const Operand&) = default;

    virtual Type GetType() const override
    {
        if (std::is_same<int8_t, T>::value)  return Type::Int8;
        if (std::is_same<int16_t, T>::value) return Type::Int16;
        if (std::is_same<int32_t, T>::value) return Type::Int32;
        if (std::is_same<float, T>::value)   return Type::Float;
        return Type::Double;
    }

    IOperand* operator+(const IOperand& other) const override
    {
        auto result = FACTORY.CreateOperand(this, &other);
        std::stringstream ss;
        ss << (enum_cast(result->GetType()) < enum_cast(Type::Float)
            ? std::stoi(ToString()) + std::stoi(other.ToString())
            : std::stod(ToString()) + std::stod(other.ToString()));
        result->FromString(ss.str());
        return result;
    }

    IOperand* operator-(const IOperand& other) const override
    {
        auto result = FACTORY.CreateOperand(this, &other);
        std::stringstream ss;
        ss << (enum_cast(result->GetType()) < enum_cast(Type::Float)
            ? std::stoi(ToString()) - std::stoi(other.ToString())
            : std::stod(ToString()) - std::stod(other.ToString()));
        result->FromString(ss.str());
        return result;
    }

    IOperand* operator*(const IOperand& other) const override
    {
        auto result = FACTORY.CreateOperand(this, &other);
        std::stringstream ss;
        ss << std::stod(ToString()) * std::stod(other.ToString());
        result->FromString(ss.str());
        return result;
    }

    IOperand* operator/(const IOperand& other) const override
    {
        auto result = FACTORY.CreateOperand(this, &other);
        std::stringstream ss;
        ss << std::stod(ToString()) / std::stod(other.ToString());
        result->FromString(ss.str());
        return result;
    }

    IOperand* operator%(const IOperand& other) const override
    {
        auto result = FACTORY.CreateOperand(this, &other);
        std::stringstream ss;
        ss << std::stoi(ToString()) % std::stoi(other.ToString());
        result->FromString(ss.str());
        return result;
    }

    std::string ToString() const override
    {
        return std::to_string(_data);
    }

    void FromString(const std::string& str) override
    {
        try
        {
            if (std::is_same<float, T>::value)
                _data = std::stof(str);
            else if (std::is_same<double, T>::value)
                _data = std::stod(str);
            else
                _data = std::stoi(str);
        }
        catch (std::out_of_range& ex)
        {
            throw Stack::Exception("value cannot be fit: " + str);
        }
    }

    template <typename D>
    Operand& operator=(D data) { _data = static_cast<T>(data); return *this; }
    Operand& operator=(Operand&&) = default;
    Operand& operator=(const Operand&) = default;

private:
    T _data = 0;
};

using Int8Operand = Operand<int8_t>;
using Int16Operand = Operand<int16_t>;
using Int32Operand = Operand<int32_t>;
using FloatOperand = Operand<float>;
using DoubleOperand = Operand<double>;
