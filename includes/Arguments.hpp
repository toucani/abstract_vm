/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Arguments.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/26 09:16:00 by dkovalch          #+#    #+#             */
/*   Updated: 2020/05/10 16:22:12 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <string>
#include <stdexcept>

class Arguments
{
public:
    Arguments() = default;
    Arguments(Arguments &&) = default;
    Arguments(const Arguments &) = default;
    Arguments(int argc, const char **argv);

    bool Help() const;
    const std::string& Filename() const;

    static void PrintHelp();

    Arguments& operator=(Arguments &&) = default;
    Arguments& operator=(const Arguments &) = default;

    class Exception : public std::runtime_error
    {
    public:
        Exception(Exception&&) = default;
        Exception(const Exception&) = default;

        explicit Exception(const char* what_arg);
        explicit Exception(const std::string& what_arg);

        Exception& operator=(Exception&&) = default;
        Exception& operator=(const Exception&) = default;
    };

private:
    static bool GetFlagValue(const std::vector<std::string>& args,
                      const std::string& shortOpt,
                      const std::string& longOpt,
                      bool defaultValue);
    static std::string GetArgValue(const std::vector<std::string>& args,
                            const std::string& shortOpt,
                            const std::string& longOpt,
                            const std::string& defaultValue = "");

    bool _help = false;
    std::string _filename = "";
};
