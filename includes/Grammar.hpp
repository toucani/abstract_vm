/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Grammar.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/28 19:39:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/06/25 20:27:11 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <tao/pegtl.hpp>

namespace Grammar
{
    namespace Values
    {
        struct IntegerNumber : tao::pegtl::seq<
                tao::pegtl::opt<tao::pegtl::one<'+', '-'>>,
                tao::pegtl::plus<tao::pegtl::digit>> {};

        struct FloatNumber   : tao::pegtl::seq<
                tao::pegtl::opt<tao::pegtl::one<'+', '-'>>,
                tao::pegtl::plus<tao::pegtl::digit>,
                tao::pegtl::opt_must<tao::pegtl::one<'.'>,
                    tao::pegtl::plus<tao::pegtl::digit>>> {};

        struct IntegerValue  : tao::pegtl::seq<tao::pegtl::sor<
                TAO_PEGTL_STRING("int8"), TAO_PEGTL_STRING("int16"), TAO_PEGTL_STRING("int32")>,
                tao::pegtl::pad<tao::pegtl::one<'('>, tao::pegtl::blank>,
                IntegerNumber,
                tao::pegtl::pad<tao::pegtl::one<')'>, tao::pegtl::blank>> {};

        struct FloatValue    : tao::pegtl::seq<tao::pegtl::sor<
                TAO_PEGTL_STRING("float"), TAO_PEGTL_STRING("double")>,
                tao::pegtl::pad<tao::pegtl::one<'('>, tao::pegtl::blank>,
                FloatNumber,
                tao::pegtl::pad<tao::pegtl::one<')'>, tao::pegtl::blank>> {};

        struct Value : tao::pegtl::seq<tao::pegtl::pad<
                        tao::pegtl::sor<IntegerValue, FloatValue>,
                        tao::pegtl::blank>> {};
    }

    namespace Commands
    {
        struct Pop   : tao::pegtl::seq<TAO_PEGTL_STRING("pop")> {};
        struct Dump  : tao::pegtl::seq<TAO_PEGTL_STRING("dump")> {};
        struct Add   : tao::pegtl::seq<TAO_PEGTL_STRING("add")> {};
        struct Sub   : tao::pegtl::seq<TAO_PEGTL_STRING("sub")> {};
        struct Mul   : tao::pegtl::seq<TAO_PEGTL_STRING("mul")> {};
        struct Div   : tao::pegtl::seq<TAO_PEGTL_STRING("div")> {};
        struct Mod   : tao::pegtl::seq<TAO_PEGTL_STRING("mod")> {};
        struct Print : tao::pegtl::seq<TAO_PEGTL_STRING("print")> {};
        struct Exit  : tao::pegtl::seq<TAO_PEGTL_STRING("exit")> {};
        struct Push  : tao::pegtl::seq<TAO_PEGTL_STRING("push"),
                tao::pegtl::plus<tao::pegtl::blank>,
                Values::Value> {};
        struct Assert  : tao::pegtl::seq<TAO_PEGTL_STRING("assert"),
                tao::pegtl::plus<tao::pegtl::blank>,
                Values::Value> {};
        struct Any   : tao::pegtl::sor<Push, Assert, Pop, Dump,
                                        Add, Sub, Mul, Div,
                                        Mod, Print, Exit> {};
    }

    struct Comment   : tao::pegtl::if_must<tao::pegtl::one<';'>,
                        tao::pegtl::until<tao::pegtl::eolf>> {};

    struct Command   : tao::pegtl::seq<
                        tao::pegtl::pad<Commands::Any, tao::pegtl::blank>,
                        tao::pegtl::sor<tao::pegtl::eolf, Comment>> {};

    struct Something : tao::pegtl::sor<Command, Comment, tao::pegtl::space> {};

    struct Code      : tao::pegtl::must<tao::pegtl::until<Commands::Exit, Something>> {};
}
