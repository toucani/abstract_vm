/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IOperand.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/03 12:41:05 by dkovalch          #+#    #+#             */
/*   Updated: 2018/06/04 21:50:02 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of abstract_vm project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <iostream>
#include <type_traits>

class IOperand
{
public:
    IOperand() = default;
    IOperand(IOperand&&) = default;
    IOperand(const IOperand&) = default;
    IOperand& operator=(IOperand&&) = default;
    IOperand& operator=(const IOperand&) = default;
    virtual ~IOperand() {};

    enum class Type { Int8, Int16, Int32, Float, Double };
    virtual Type GetType() const = 0;

    virtual IOperand* operator+(const IOperand&) const = 0;
    virtual IOperand* operator-(const IOperand&) const = 0;
    virtual IOperand* operator*(const IOperand&) const = 0;
    virtual IOperand* operator/(const IOperand&) const = 0;
    virtual IOperand* operator%(const IOperand&) const = 0;

    virtual std::string ToString() const = 0;
    virtual void FromString(const std::string& ) = 0;

    bool operator!=(const IOperand&) const;
    bool operator==(const IOperand&) const;
};

template <typename T>
constexpr auto enum_cast(T enumValue)
{
    return static_cast<std::underlying_type_t<T>>(enumValue);
}

std::ostream& operator<<(std::ostream&, const IOperand&);
std::ostream& operator<<(std::ostream&, const IOperand*);
