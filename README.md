# abstract vm

Abstract_vm is a simple stack-based virtual machine capable of running special code. Use ```--help``` to learn more.

## Installing

Clone this repository recursively:
```
git clone --recurse-submodules
```
, move into the folder
```
cd abstract_vm && mkdir build && cd build
```
and
```
cmake .. && make
```

It can be build with sanitizers, just by using:
```
cmake .. -DSANITIZE_ADDRESS=On && make
```
, or
```
cmake .. -DSANITIZE_MEMORY=On && make
```
, or
```
cmake .. -DSANITIZE_THREAD=On && make
```
, or
```
cmake .. -DSANITIZE_UNDEFINED=On && make
```
instead of the last step.


## Using

## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=abstract_vm from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to [sanitizers for cmake](https://github.com/arsenm/sanitizers-cmake/)
* to [Google test](https://github.com/google/googletest)
* to UNIT Factory, for inspiration to do our best.
* to all UNIT Factory students, who shared their knowledge and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kyiv) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
